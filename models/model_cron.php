<?php

class Model_cron extends CI_Model
{

  public function do_something()
  {
    /*
     * Whatever periodic admin things you do, ie:
     */
    $sql = "UPDATE myTable SET colA=val WHERE (colB=valB AND DATE_SUB(now(),INTERVAL 2 WEEK)>last_updated)";
    $this->db->query($sql);
    return;
  }

}
