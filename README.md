# About
This is one way of running admin tasks on a codeigniter site hosted on MS Azure, using WebJobs to run scheduled jobs.
  
The advantage of this is that you can develop your scheduled jobs using the same MVC pattern as the rest of your site, and use a simple cron *get* request to trigger it within the Azure environment.

# Instructions:

1. Create controller and model for your scheduled tasks as per the example
2. Create a .php file as per the cron_curl.php example - this isn't part of the CI structure, you keep this on your desktop to upload to Azure in step 5.
3. In the Azure Portal, select your App Service from your dashboard
4. In the App Service Blade, on the left menu under *Settings* you'll find the option for *WebJobs*. Click that.
5. In the *WebJobs* settings:
    + click *Add* and give your job a meaningful name to you
    + Upload the cron_curl.php file you created
    + Select the *Type* as *Triggered*
    + Select the *Triggers* as *Scheduled*
    + Enter the *CRON Expression* for the frequency you want the job to run (btw, Azure uses seconds in its cron format, so daily would be 0 m h * * * - in my case I have it run at 0101hrs daily, or 0 1 1 * * *)
    + Click **OK** and the WebJob will be validated and saved, showing in your list of scheduled jobs

You can test your job by triggering it manually (select the job from the list and click the play button in the WebJobs blade.

In my case I have the job running silently; there's no view, no return values, nothing to give any feedback as to what has happened; if you wanted to, you could easily extend your controller to handle errors more gracefully, log successes/failures, etc.
