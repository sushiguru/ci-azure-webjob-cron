<?php
$curl = curl_init();
curl_setopt_array(
  $curl, array(
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => 'your-site-url/cron/do_something'
  )
);
curl_exec($curl);
curl_close($curl);