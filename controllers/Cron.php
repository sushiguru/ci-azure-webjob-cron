<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {


  public function __construct()
  {
    parent::__construct();
    $this->load->model('model_cron');
  }

  public function index()
  {
    /*
     * If you just wanted something to happen with no specific methods just use the index function
     * for something to happen by default
     */
  }

  public function do_something()
  {
    $this->model_cron->do_something();
  }

}


